<?php 



class MLDataExport 
{   protected $recordCount;
	protected $table;
	protected $DocGuid; 
	protected $data;
	function __construct($table = "", $recordCount="")
	{
		$this->table = $table;
		$this->recordCount = $recordCount;
		$this->data = [];
	}
	public function getData()
	{
		return $this->data;
	}
	
	public function setDocGuid($DocGuid)
	{
		$this->DocGuid = $DocGuid;
	}
	
	public function getDocGuid()
	{
		return $this->DocGuid;
	}
	
	public function getTable()
	{
		return $this->table;
	}
	
	public function getRecordCount()
	{
		return $this->recordCount;
	}
	
	public function SetData($MLDataString)
	{   $R = "OK";
	    try
		{
		$MLdata = explode("\r\n", substr($MLDataString, 0, -2));
		foreach ($MLdata as &$value) {
			if (strlen($value)>0){
			$this->data[] = $value;
			}
			}
		unset($value);
		}
		catch (Exception $e) {
		$R = "ER ". $e->getMessage();	
		}
		return $R;
	}
}


?>