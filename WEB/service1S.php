<?php 


class service1S
 {
	private $client = null;
	private $servName = "";
	
	function __construct($dbName,$login,$password,$Operation){
		$this->servName = $dbName;
		try {
		  $this->client = new SoapClient($dbName , array('login' => $login, 'password' => $password, 'exceptions' => 1)); 	
		}
		catch (Exception $e) {
		
		  $res = $Operation."&MLResult=ER невозможно подключится к базе 1С";
		  echo mb_convert_encoding($res, 'CP1251', mb_detect_encoding($res));
		  die;
		  }
	}
	public function getClient(){
		return $this->client;
	}
	
	public function loadDocument($docGuid,$SerieNum){
		if (is_string($docGuid)) {
		$params["docGuid"] = $docGuid;
		$params["SerieNum"] = $SerieNum;
		$result = $this->client->loadDocHTTP($params);
		return $result->return;
		} else {
			$params["MLDeviceID"] = "";
			$params["Data"] = $SerieNum;
			$result = $this->client->loadDocs($params);
			return $result->return;
		}
	}
	public function loadGoodDocument($docGuid,$ArrayData){
	    $params["docGuid"] = $docGuid;
		$params["arrayData"] = $ArrayData;
		$result = $this->client->loadGoodDoc($params);
		return $result->return;	
	}	
	
	public function loadExch($MLDeviceID,$Data){
			$params["MLDeviceID"] = $MLDeviceID;
			$params["Data"] = $Data;
			$result = $this->client->loadExch($params);
			return $result->return;
    }	
	public function GetDocLocation($BarCode,$MLDeviceID){
		
		$params["BarCode"] = $BarCode;
		$params["TSD"] = $MLDeviceID;
		$result = $this->client->GetDocLocation($params);
		return $result;
	}
	
	public function getTaskCompression($CellGuid,$MLDeviceID){
		
		$params["CellGuid"] = $CellGuid;
		$params["TSD"] = $MLDeviceID;
		$result = $this->client->getTaskCompression($params);
		return $result;
	}
	
	public function  unloadDocument($Table,$docGuid,$MLDeviceID){	
	switch ($Table) {
	  	
	  case 0 :{
		 //документ
		if ($docGuid==0) {
		  $params["MLDeviceID"] = $MLDeviceID;
		  $result = $this->client->unloadTSDDocHeader($params);	
		} else {
		  $params["docGuid"] = $docGuid;
		  $result = $this->client->unloadDocHeaderHTTP($params);
		}
		return $result->return;
	  }
	  case 1 :{
		  //СтрокиДокумента
		if ($docGuid==0) {
		  $params["MLDeviceID"] = $MLDeviceID;
		  $result = $this->client->unloadTSDDocGood($params);	
		} else {  
		  $params["docGuid"] = $docGuid;
		  $result = $this->client->unloadDocGoodHTTP($params);
		}
		return $result->return;
	  }
	  case 2 : {
		  //ОстаткиПоЯчейкам
		 $params["MLDeviceID"] = $MLDeviceID;  
		$result = $this->client->unloadCellRest($params);
		return $result->return; 
	  }
	 /* case 3 : {
		  //НастройкиОбмена
		  //обмен идет через драйвер по запросу из ТСД при открытой форме справочника ТСД в 1С 
	  }*/
	  case 4 :{	
	    //спрШтрихкоды
		
		$result = $this->client->unloadBarcodeHTTP();
		return $result->return;
	  }
	  case 5 : {
		 //ШтрихкодыПолныеДокумента 
		if ($docGuid==0) {
		  $params["MLDeviceID"] = $MLDeviceID;
		  $result = $this->client->unloadTSDDocSeries($params);	
		} else { 
		$params["docGuid"] = $docGuid;
		$result = $this->client->unloadDocSeriesHTTP($params);
		}
		return $result->return;
	  }
	  case 6 : {
		  //Ячейки
		$params["MLDeviceID"] = $MLDeviceID;  
		$result = $this->client->unloadCellHTTP($params);
		return $result->return;
	  } 
	case 7 : {
		$params["MLDeviceID"] = $MLDeviceID;  
		$result = $this->client->unloadTSDPalett($params);
		return $result->return;
	  } 
	 case 8 : {
		  //ДокументыДляЗагрузки
		$params["MLDeviceID"] = $MLDeviceID;
		$result = $this->client->unloadDocSeriesHTTP($params);
		return $result->return;
	  } 
	 case 9 : {
		  // Ряды
		$params["MLDeviceID"] = $MLDeviceID;
		$result = $this->client->unloadRowHTTP($params);
		return $result->return;
	  }  
	 case 10 : {
		  //Этажи
		$params["MLDeviceID"] = $MLDeviceID;
		$result = $this->client->unloadFloorHTTP($params);
		return $result->return;
	  }  
	 case 11 : {
		  //Секторы
		$params["MLDeviceID"] = $MLDeviceID;
		$result = $this->client->unloadSectorHTTP($params);
		return $result->return;  
	  }  
	 case 12 : {
		  //Колонка
		$params["MLDeviceID"] = $MLDeviceID;
		$result = $this->client->unloadColumnHTTP($params);
		return $result->return;
	  }  
	 /*case 13 : {
		  //ШКТранспортныеЕдиницы
	  }  */
	 case 14 : {
		  //Сообщения
		
		$params["MLDeviceID"] = $MLDeviceID;
		$result = $this->client->unloadTSDExch($params);
		return $result->return;
	  }  
	 default : {
		 return  "ER для данной таблицы выгрузка не предусмотрена";
	 }
  	 } 
	}
  }
?> 
