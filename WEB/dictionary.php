<?php 

spl_autoload_register(function ($ClassName) {
	$filePath = str_replace('\\', DIRECTORY_SEPARATOR, __DIR__ . '\\' . 	$ClassName . '.php');
	if (file_exists($filePath)) {
		include $filePath;
	}
});


include("setting.php");

session_start();
ini_set("soap.wsdl_cache_enabled", "0"); 


//переменная нужна для ведения лога, потом можно выставить в 0
$_SESSION["MLlog"] = $_log;


if ($_SESSION["MLlog"]) {  
   if (isset($_POST["MLSessionID"])) {
			$file = "files/Log".$_POST["MLSessionID"].".txt";  
			} else {$file = "files/LogQ.txt";  }
			file_put_contents($file, date("d.m.Y H:i:s"),FILE_APPEND | LOCK_EX);
			file_put_contents($file, "\r\n", FILE_APPEND | LOCK_EX);
			file_put_contents($file, "POST", FILE_APPEND | LOCK_EX);
			file_put_contents($file, "\r\n", FILE_APPEND | LOCK_EX);
			foreach ($_POST as $key => $value) {
				file_put_contents($file, $key."=>".$value, FILE_APPEND | LOCK_EX);
				file_put_contents($file, "\r\n", FILE_APPEND | LOCK_EX);
			}	
} 


$res = "";
$MLResult="OK";

if (isset($_POST["MLOpCode"])){
	//Обмен по HTTP протокулу с ТСД
	switch  ($_POST["MLOpCode"]) {	
	case  "OpExpDataInit" : {
		// инициализация обмена ТСД->1C
 	  $_SESSION["MLSessionID"] = $_POST["MLSessionID"];
	  $_SESSION["MLDeviceID"] = $_POST["MLDeviceID"];
	  $_SESSION["MLTblId"] = $_POST["MLTblId"];
	  $_SESSION["MLRecCount"] = $_POST["MLRecCount"];
	  //создали объект в котором будем накапливать данные для последующей передачи в 1С  
	  $_SESSION["MLDataExport"] = new MLDataExport( $_POST["MLTblId"],$_POST["MLRecCount"]);
	  if (isset($_POST["MLDocGuid"])) 
	  {
		$_SESSION["MLDataExport"]->setDocGuid($_POST["MLDocGuid"]); 
      } else {
		$_SESSION["MLDataExport"]->setDocGuid(0); 
	  }
	  file_put_contents($file,"docGUID", FILE_APPEND | LOCK_EX);
	  file_put_contents($file, $_SESSION["MLDataExport"]->getDocGuid() , FILE_APPEND | LOCK_EX);
	  file_put_contents($file, "\r\n", FILE_APPEND | LOCK_EX);
	  $res = "MLOpCode=OpExpDataInitAns&MLResult=".$MLResult;
	  break;
	  } 
	case  "OpExpData" : {
	  //принимаем пачки данных от ТСД	
 	  $_SESSION["MLFirstRec"] = $_POST["MLFirstRec"];
	  $ex = $_SESSION["MLDataExport"];
	  file_put_contents($file, "docGUID" , FILE_APPEND | LOCK_EX);
	  file_put_contents($file, $ex->getDocGuid() , FILE_APPEND | LOCK_EX);
	  file_put_contents($file, "\r\n", FILE_APPEND | LOCK_EX);
	  $ex->SetData(mb_convert_encoding($_POST["MLData"], 'UTF-8','CP1251' ));
	  
	  if (isset($_POST["MLLastExp"])) {
		// последняя порция получена, теперь можно отправить в 1С 
		$WebSer1S =  new service1S($Addr_1C,$Login_1C,$Pass_1C,"MLOpCode=OpExpDataAns");
		switch ($ex->getTable()) {
		case 1 :{
				try{
		    //для таблицы строк документа (уплотнение)
			$MLResult = $WebSer1S->loadGoodDocument($ex->getDocGuid(),$ex->getData());

			}
			catch (Exception $e) {
				$MLResult = "ER ". $e->getMessage();
				
				}
		break;
		};	  	
		case 5 :{
				try{
		    //для таблицы со штрихкодами по документу	(набор, размещение)	
			$MLResult = $WebSer1S->loadDocument($ex->getDocGuid(),$ex->getData());

			}
			catch (Exception $e) {
				$MLResult = "ER ". $e->getMessage();			
				}
		break;
		};
		
		  case 14:{
			try{
		    //таблица сообщений	
			echo "14 loadExch";
			$MLResult = $WebSer1S->loadExch($_SESSION["MLDeviceID"],$ex->getData());

			}
			catch (Exception $e) {
				$MLResult = "ER ". $e->getMessage();
				
				}
			break;
		};
		default:{
			$MLResult = "ER Выгрузка не предусмотрена";
		}
		}
		unset($WebSer1S);
		session_destroy();
	  }
	  $res = "MLOpCode=OpExpDataAns&MLResult=".$MLResult."&MLFirstRec=".$_POST["MLFirstRec"];
	  break;
	  }  
	case  "OpImpDataInit" : {
		//Инициализируем обмен 1С->ТСД 
		// реально обмениваться только полными таблицами из ТСД не выгружаются доп параметры, так что работать работает но пользоваться не получилось пришлось делать монстра
	  $_SESSION["MLSessionID"] = $_POST["MLSessionID"];
	  $_SESSION["MLDeviceID"] = $_POST["MLDeviceID"];
	  $_SESSION["MLTblId"] = $_POST["MLTblId"];
	  $_SESSION["MLDataImport"] = new MLDataImport($_POST["MLTblId"]);

	  if (isset($_SESSION["MLDocGuid"])) 
	  {
		  $_SESSION["MLDataImport"]->setDocGuid($_SESSION["MLDocGuid"]);  
	  } else
      {
		  $_SESSION["MLDataImport"]->setDocGuid("0");
	  }		  
	  //подключаемся к 1С	
		$WebSer1S =  new service1S($Addr_1C,$Login_1C,$Pass_1C,"MLOpCode=OpExpDataAns");
	  //запрашиваем данные
	  $Data = $WebSer1S->unloadDocument($_SESSION["MLDataImport"]->getTable(),$_SESSION["MLDataImport"]->getDocGuid(),$_SESSION["MLDeviceID"]);
	  if (is_object($Data)) {
		  if ($Data->result=="OK") {
		    $MLResult = $_SESSION["MLDataImport"]->setData($Data->ArrayProperty);
		  }
		  else {
			$MLResult =   $Data->result;
		  }            			  
         }
		 else {
		  $MLResult = $Data;	
		}	
	  unset($WebSer1S);
      $res = "MLOpCode=OpImpDataInitAns&MLResult=".$MLResult."&MLRecCount=".$_SESSION["MLDataImport"]->getRecordCount();
	  break;
	}
	case  "OpImpData" : {
  	   //выгружаем данные пачками в ТСД           
                $recsInPacket = 0;
                $precFirstRec = $_SESSION["MLDataImport"]->getCurrentRecord();		
                $packetData = $_SESSION["MLDataImport"]->Process();
	            $recsInPacket = $_SESSION["MLDataImport"]->getCurrentRecord() - $precFirstRec;
				if (($precFirstRec + $recsInPacket) >= $_SESSION["MLDataImport"]->getRecordCount()) {
					session_destroy();
				//выгружена последняя порция. сессию завершаем.
				}
    
		$res = "MLOpCode=OpImpDataAns&MLFirstRec=".$precFirstRec."&MLRecCount=".$recsInPacket."&MLData=".$packetData."&MLResult=OK";
		break;
	}
	
	}
}
elseif (isset($_POST["MLQuery"])){
    //запрашиваем у 1С информацию о документе размещения по штрих коду пакета , в ТСД приходит информация о GUID документа в 1С документ регистрируется для обмена с ТСД, потом с ТСД инициируется загрузка данных
	if (isset($_POST["BarCode"])&&isset($_POST["placing"])) {
		//работаю без объекта, почему то в ТСД через объект приходят начальные спец символы в ответе, откуда берутся разбираться уже некогда.
	$client = new SoapClient($Addr_1C , array('login' => $Login_1C, 'password' => $Pass_1C, 'exceptions' => 1)); 	
	
	$params["BarCode"] = $_POST["BarCode"]; 
	$params["MLDeviceID"] = $_POST["MLDeviceID"];
	$res = $client->GetDocLocation($params)->return;
	}
	//запрашиваем у 1С информацию на уплотнение по коду ячейки, в 1С формируется документ на уплотнение, в ТСД возвращается GUID документа, из ТСД инициируется обмен данными 
	if (isset($_POST["Compression"])) {
		
	$client = new SoapClient($Addr_1C , array('login' => $Login_1C, 'password' => $Pass_1C, 'exceptions' => 1)); 	
	
	$params["CellGuid"] = $_POST["CellGuid"]; 
	$params["MLDeviceID"] = $_POST["MLDeviceID"];
	$res = $client->getTaskCompression($params)->return;
	}
	
}
elseif (isset($_POST["MLDelFile"])){
	
	//удаляем файл с FTP по запросу с ТСД после загрузки в ТСД.
	$conn_id = ftp_connect($Addr_FTP);
	// вход с именем пользователя и паролем
	$login_result = ftp_login($conn_id, $Login_FTP, $Pass_FTP);
	// попытка удалить файл
	if (ftp_delete($conn_id, $_POST["MLFileName"])) {
		$res = "файл удален";
	} else {
		$res = "файл не удален";
	}

	// закрытие соединения
	ftp_close($conn_id);
}
else { 
  // непонятный запрос ничего не обрабатываем.
 $res = "MLResult=ER xxx";  
 session_destroy();
}
if ($_SESSION["MLlog"]) {  
	file_put_contents($file, date("d.m.Y H:i:s"), FILE_APPEND | LOCK_EX);
	file_put_contents($file, "\r\n", FILE_APPEND | LOCK_EX);		
	file_put_contents($file, mb_convert_encoding($res, 'CP1251', mb_detect_encoding($res)), FILE_APPEND | LOCK_EX);
	file_put_contents($file, "\r\n", FILE_APPEND | LOCK_EX);
	}
//Результат выполнения для передачи в ТСД.
echo mb_convert_encoding($res, 'CP1251', mb_detect_encoding($res));

?>
