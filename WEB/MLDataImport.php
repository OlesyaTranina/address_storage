<?php 



class MLDataImport
{   protected $recordCount;
	protected $table;
	protected $DocGuid; 
	protected $data;
	protected $avgPackSize = 20000;
	protected $CurrentRecord;
	function __construct($table = "")
	{
		$this->table = $table;
		$this->recordCount = 0;
		$this->CurrentRecord = 0;
		$this->data = [];
	}
	public function getData()
	{
		return $this->data;
	}
	
	public function getDocGuid()
	{
		return $this->DocGuid;
	}
	
	public function setDocGuid($DocGuid)
	{
		$this->DocGuid = $DocGuid;
	}
	
	public function getTable()
	{
		return $this->table;
	}
	
	public function getRecordCount()
	{
		return $this->recordCount;
	}
	
	public function getCurrentRecord()
	{
		return $this->CurrentRecord;
	}
	
	
	public function setData($Data1C)
	{
		$R = "OK";
	    try
		{
	    if (is_string($Data1C)) {
			$this->data[] = $Data1C;
		} else {		
		foreach ($Data1C as &$value) {
			$this->data[] = $value;
			}
		unset($value);	
		}
		$this->recordCount = count($this->data);
		}
		catch (Exception $e) {
		$R = "ER ". $e->getMessage();	
		}
		return $R;
	}
	public function Process()
        {
            $result = "";
            $recsInPacket = 0;
			
            while (strlen($result) < $this->avgPackSize)
            {
			
			if ($this->CurrentRecord >= Count($this->data)){
                    break;
				}
				
				$result = $result.$this->data[$this->CurrentRecord]."\r\n";
			    $recsInPacket++;
                $this->CurrentRecord++;
            }
			
            return $result;
        }
}


?>